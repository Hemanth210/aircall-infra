# Define the provider
provider "aws" {
  region = "eu-central-1"
  access_key = "AKIAWA7WT3LBGDHA6JPD"
  secret_key = "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"
}

# Define the variables
variable "app_name" {
  default = "imageresize"
}

variable "aws_region" {
  default = "eu-central-1"
}

# Create the S3 bucket
resource "aws_s3_bucket" "image_resize_bucket" {
  bucket = "${var.app_name}-bucket"
}
resource "aws_s3_bucket_public_access_block" "image_resize_bucket" {
  bucket = aws_s3_bucket.image_resize_bucket.id

  block_public_acls = false
  block_public_policy = false 
  ignore_public_acls = false
  restrict_public_buckets = false
}

# Create the IAM role for Lambda
resource "aws_iam_role" "image_resize_lambda_role" {
  name = "${var.app_name}-lambda-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
        Action = "sts:AssumeRole"
      }
    ]
  })
}

# Attach the S3 policy to the IAM role
resource "aws_iam_role_policy_attachment" "image_resize_lambda_role_policy_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
  role       = aws_iam_role.image_resize_lambda_role.name
}

# Create the AWS Lambda function
resource "aws_lambda_function" "image_resize_lambda_function" {
  filename         = "./app.zip"
  function_name    = "${var.app_name}-function"
  role             = aws_iam_role.image_resize_lambda_role.arn
  handler          = "app.handler"
  runtime          = "nodejs14.x"
  timeout          = 10
  memory_size      = 128
  source_code_hash = filebase64sha256("./app.zip")
  #source_code_hash = "hashicorp/random/length:latest"
  #image_uri = "hemanth210/image-resize-aircall:latest"

  environment {
    variables = {
      S3_BUCKET = aws_s3_bucket.image_resize_bucket.bucket
    }
  }
}

# Create the API Gateway REST API
resource "aws_api_gateway_rest_api" "image_resize_api" {
  name        = "image-resize-api"
  description = "API Gateway for Image Resize"
}

# Create the API Gateway resource
resource "aws_api_gateway_resource" "image_resize_resource" {
  rest_api_id = aws_api_gateway_rest_api.image_resize_api.id
  parent_id   = aws_api_gateway_rest_api.image_resize_api.root_resource_id
  path_part   = "image"
}

# Create the API Gateway method
resource "aws_api_gateway_method" "image_resize_method" {
  rest_api_id   = aws_api_gateway_rest_api.image_resize_api.id
  resource_id   = aws_api_gateway_resource.image_resize_resource.id
  http_method   = "POST"
  authorization = "NONE"
}

# Create the API Gateway integration
resource "aws_api_gateway_integration" "image_resize_integration" {
  rest_api_id             = aws_api_gateway_rest_api.image_resize_api.id
  resource_id             = aws_api_gateway_resource.image_resize_resource.id
  http_method             = aws_api_gateway_method.image_resize_method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "arn:aws:apigateway:${var.aws_region}:lambda:path/2015-03-31/functions/${aws_lambda_function.image_resize_lambda_function.arn}/invocations"
}

# Create the API Gateway deployment
resource "aws_api_gateway_deployment" "image_resize_deployment" {
  rest_api_id = aws_api_gateway_rest_api.image_resize_api.id
  stage_name  = "prod"
}

# Output the API Gateway URL
output "api_gateway_url" {
  value = "https://${aws_api_gateway_rest_api.image_resize_api.id}.execute-api.${var.aws_region}.amazonaws.com/prod/image"
}
